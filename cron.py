"""
Copyright (C) 2015 Corentin Bocquillon <corentin@nybble.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""

import datetime
import time
from os import system
from sys import exit

def decode(chaine):
    tt = chaine.split(" ")
    nbSplit = len(tt)

    #Si le nombre d'arguments (dans le fichier crontab) est inférieur à 6
    #Il est invalide, on retourne donc une erreur
    if nbSplit < 6:
        return -1

    maCommande = ""
    if nbSplit > 4:
        for i in range(5,nbSplit):
            maCommande += tt[i] + " "
        maCommande = maCommande[0:(len(maCommande)-1)]
        maCommande = maCommande.replace('\n', '')
    try:
        if tt[0]!='*':
            int(tt[0])
    except:
        return -1
    return tt[0], tt[1], tt[2], tt[3], tt[4], maCommande

def traitement(job,date):
    if job[0]=='*' or job[0]==date[0]:
        if job[1]=='*' or job[1]==date[1]:
            if job[2]=='*' or job[2]==date[2]:
                if job[3]=='*' or job[3]==date[3]:
                    if job[4]=='*' or job[4]==date[4]:
                        system(job[5])

crontab = open("crontab", 'r')
jobs = []

while True:
    chaine = crontab.readline()
    if chaine=="":
        break
    else:
        if chaine[0:1]!='#':
            infos = decode(chaine)
            if infos==-1:
                exit("Fichier crontab incorrecte")
            jobs.append(infos)

maintenant = datetime.datetime.now()
tt = maintenant.timetuple()

annee = tt[0]
mois = tt[1]
jour = tt[2]
heure = tt[3]
minute = tt[4]

while True:
    for element in jobs:
        traitement(element,tt)
    print(jobs)
    time.sleep(60)
